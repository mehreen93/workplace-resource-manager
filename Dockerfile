FROM gradle:jdk10 as builder

COPY --chown=gradle:gradle . /home/gradle/src

WORKDIR /home/gradle/src

RUN gradle clean build --stacktrace

FROM tomcat:latest

COPY --from=builder /home/gradle/src/build/libs/src-1.0.war /usr/local/tomcat/webapps

EXPOSE 8080

CMD ["catalina.sh", "run"]
