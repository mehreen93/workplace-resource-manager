package net.mehreen.web.controller;

import net.mehreen.domain.Attendance;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class CheckInController {

    public static final String COMMAND_NAME = "timetracker";
    public static final String SIGN_IN_PAGE = "timetracker";

    @RequestMapping(value = "/timetracker", method = RequestMethod.GET)
    public String checkinIndex(ModelMap model) {
        Attendance attendance = new Attendance();

        setupReferenceData(attendance, model);

        return SIGN_IN_PAGE;

    }
//
//    public String checkIn(@Valid @ModelAttribute(COMMAND_NAME) Attendance attendance,
//                          BindingResult bindingResult,
//                          ModelMap model,)

    private void setupReferenceData(Attendance attendance, ModelMap model) {
        model.put(COMMAND_NAME, attendance);
    }
}
