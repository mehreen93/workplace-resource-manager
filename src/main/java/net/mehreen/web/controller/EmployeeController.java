package net.mehreen.web.controller;

import net.mehreen.dao.EmployeeDao;
import net.mehreen.domain.Employee;
import net.mehreen.service.EmployeeService;
import net.mehreen.web.validator.EmployeeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class EmployeeController {

    public static final String COMMAND_NAME = "employee";
    public static final String VIEW_PAGE = "employee/employee";

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private EmployeeValidator employeeValidator;

    @InitBinder(COMMAND_NAME)
    public void init(WebDataBinder webDataBinder){
        webDataBinder.addValidators(employeeValidator);
    }

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public String addEmployee(ModelMap model) { //show

        Employee employee = new Employee();

        setUpReference(employee, model);

        return VIEW_PAGE;
    }

    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    public String submitEmployee(@Valid @ModelAttribute(COMMAND_NAME) Employee employee,
                                 BindingResult result,
                                 ModelMap model) {

        if (result.hasErrors()) {
            setUpReference(employee, model);
            return VIEW_PAGE;
        }
        employeeService.save(employee);
        return "redirect:" + "/employee";
    }

    private void setUpReference(Employee employee, ModelMap model) {
        model.put(COMMAND_NAME, employee);
        model.put("employeeList", employeeDao.getAll());
    }
}
