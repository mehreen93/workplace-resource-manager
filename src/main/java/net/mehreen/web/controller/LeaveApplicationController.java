package net.mehreen.web.controller;

import net.mehreen.dao.EmployeeDao;
import net.mehreen.domain.Employee;
import net.mehreen.domain.LeaveApplication;
import net.mehreen.domain.LeaveDuration;
import net.mehreen.domain.LeaveType;
import net.mehreen.service.LeaveApplicationService;
import net.mehreen.web.editor.EmployeeEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class LeaveApplicationController {

    public static final String COMMAND_NAME = "leaveApplication";
    private static final String VIEW_PAGE = "leaveApplication/showLeaveApplication";

    @Autowired
    private LeaveApplicationService leaveApplicationService;

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private EmployeeEditor employeeEditor;

    @InitBinder(COMMAND_NAME)
    public void init(WebDataBinder webDataBinder){
        webDataBinder.registerCustomEditor(Employee.class, employeeEditor);
    }

    @RequestMapping(value = "/leaveApplication", method = RequestMethod.GET) //handler
    public String showLeaveApplication(ModelMap model) {
        LeaveApplication leaveApplication = new LeaveApplication();

        setupReferenceData(leaveApplication, model);

        return VIEW_PAGE;
    }

    @RequestMapping(value = "/leaveApplication", method = RequestMethod.POST)
    public String submitLeaveApplication(@ModelAttribute(COMMAND_NAME) LeaveApplication leaveApplication,
                                         RedirectAttributes redirectAttributes) {
        System.out.println("before....");
        leaveApplicationService.save(leaveApplication);
        System.out.println("after....");
        redirectAttributes.addFlashAttribute("messgae", "saved!");
        return "redirect:" + "/leaveApplication";
    }

    private void setupReferenceData(LeaveApplication leaveApplication, ModelMap modelMap) {
        modelMap.put(COMMAND_NAME, leaveApplication);
        modelMap.put("leaveTypes", LeaveType.values());
        modelMap.put("leaveDurations", LeaveDuration.values());
        modelMap.put("employeeList", employeeDao.getAll());
    }
}
