package net.mehreen.web.validator;

import net.mehreen.domain.Employee;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class EmployeeValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Employee.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Employee employee = (Employee)target;
        if (!employee.getContactNo().substring(0, 4).equals("+880")) {
            errors.rejectValue("contactNo", "error.employee.contact.no");
        }
    }
}
