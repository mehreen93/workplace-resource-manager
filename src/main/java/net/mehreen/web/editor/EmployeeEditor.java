package net.mehreen.web.editor;

import net.mehreen.dao.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;

@Component
public class EmployeeEditor extends PropertyEditorSupport {

    @Autowired
    private EmployeeDao employeeDao;

    @Override
    public String getAsText() {
        return super.getAsText();
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        long id = Long.valueOf(text);
        setValue(employeeDao.getEmployee(id));
    }


}
