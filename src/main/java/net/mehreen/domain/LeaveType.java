package net.mehreen.domain;

public enum LeaveType {

    CASUAL_LEAVE("Casual Leave"),
    MEDICAL_LEAVE("Medical Leave");

    private String displayName;

    LeaveType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
