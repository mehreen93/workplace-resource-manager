package net.mehreen.domain;

public enum LeaveDuration {

    FULL_DAY("Full Day"),
    FIRST_HALF("First Half"),
    SECOND_HALF("Second Half");

    private String displayName;

    LeaveDuration(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
