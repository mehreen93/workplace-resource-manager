package net.mehreen.dao;

import net.mehreen.domain.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class EmployeeDao {

    @PersistenceContext(unitName = "unitOne")
    private EntityManager entityManager;

    @Transactional
    public void saveOrUpdate(Employee employee) {
        if (employee.getId() == 0) {
            entityManager.persist(employee);
            entityManager.flush();
        } else {
            entityManager.merge(employee);
        }
    }

    public List<Employee> getAll() {
        String jpql = "from Employee";
        TypedQuery<Employee> query = entityManager.createQuery(jpql, Employee.class);
        return query.getResultList();
    }

    public Employee getEmployee(long id){
        return entityManager.find(Employee.class, id);
    }
}
