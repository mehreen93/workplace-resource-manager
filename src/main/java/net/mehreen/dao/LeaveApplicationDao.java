package net.mehreen.dao;

import net.mehreen.domain.Employee;
import net.mehreen.domain.LeaveApplication;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class LeaveApplicationDao {

    @PersistenceContext(unitName = "unitOne")
    private EntityManager entityManager;

    @Transactional
    public void saveOrUpdate(LeaveApplication leaveApplication) {
        if (leaveApplication.getId() == 0) {
            entityManager.persist(leaveApplication);
            entityManager.flush();
        } else {
            entityManager.merge(leaveApplication);
        }
    }

//    public List<Employee> getAll() {
//        String jpql = "from Employee";
//        TypedQuery<Employee> query = entityManager.createQuery(jpql, Employee.class);
//        return query.getResultList();
//    }
}
