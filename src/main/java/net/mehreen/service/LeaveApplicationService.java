package net.mehreen.service;

import net.mehreen.dao.LeaveApplicationDao;
import net.mehreen.domain.LeaveApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LeaveApplicationService {

    @Autowired
    private LeaveApplicationDao leaveApplicationDao;

    @Transactional
    public void save(LeaveApplication leaveApplication) {
        leaveApplicationDao.saveOrUpdate(leaveApplication);
    }
}
