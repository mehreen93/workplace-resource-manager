package net.mehreen.service;

import net.mehreen.dao.EmployeeDao;
import net.mehreen.domain.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeDao employeeDao;

    @Transactional
    public void save(Employee employee) {
        employeeDao.saveOrUpdate(employee);
    }
}
