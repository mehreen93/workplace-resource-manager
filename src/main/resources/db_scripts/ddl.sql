CREATE table leave_application(
  id serial primary key,
  employee_id bigint,
  leave_type varchar,
  leave_duration varchar,
  start_date timestamp,
  end_date timestamp,
  reason varchar
)

CREATE table employee(
  id serial primary key,
  name varchar,
  leave_type varchar,
  address varchar
)

alter table employee rename leave_type to contact_no

alter table employee add column version integer

select * from leave_application