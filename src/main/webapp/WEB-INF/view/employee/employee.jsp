<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title><fmt:message key="employee.add"/></title>
</head>
<body>
    <c:if test="${not empty messgae}">
        <c:out value="${messgae}"/>
    </c:if>

    <form:form commandName="employee" method="post">
        <div>
            <form:label path="name">
                <fmt:message key="name"/>
            </form:label>
            <form:input path="name"/>
            <form:errors path="name"/>
        </div>
        <div>
            <form:label path="contactNo">
                <fmt:message key="contact.no"/>
            </form:label>
            <form:input path="contactNo"/>
            <form:errors path="contactNo"/>
        </div>
        <div>
            <form:label path="address">
                <fmt:message key="address"/>
            </form:label>
            <form:input path="address"/>
            <form:errors path="address"/>
        </div>
        <div>
            <input type="submit" value="Submit" name="submit"/>
        </div>
    </form:form>

    <div>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Contact No</th>
                <th>Address</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${employeeList}" var = "employee">
                <tr>
                    <td>${employee.name}</td>
                    <td>${employee.contactNo}</td>
                    <td>${employee.address}</td>
                    <td></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
</body>
</html>
