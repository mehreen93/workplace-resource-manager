<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title><fmt:message key="leave.application.title"/></title>
</head>
<body>
    <c:if test="${not empty messgae}">
        <c:out value="${messgae}"/>
    </c:if>

    <form:form commandName="leaveApplication" method="post">
        <div>
            <form:label path="employee">
                <fmt:message key="employee.name"/>
            </form:label>
            <form:select path="employee"
                         items="${employeeList}"
                         itemLabel="name"
                         itemValue="id"/>
        </div>
        <div>
            <form:label path="leaveType">
                <fmt:message key="leave.type"/>
            </form:label>
            <form:select path="leaveType"
                         items="${leaveTypes}"
                         itemLabel="displayName"/>
        </div>
        <div>
            <form:label path="reason">
                <fmt:message key="reason"/>
            </form:label>
            <form:input path="reason"/>
        </div>
        <div>
            <input type="submit" value="Submit" name="submit"/>
        </div>
    </form:form>
</body>
</html>
